#ifndef TIMER_H
#define TIMER_H

#define TICKS_PER_SEC 15625.0

void timerInit();

void resetTimer();

unsigned long readTimer();

double readTimer_ms();

double toMS(int count);


#endif 

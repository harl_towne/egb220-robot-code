#include "Timer.h"

void timerInit()
{
  //select clock internal
  //prescaler for 1 ish ms timesteps
  TCCR3A = 0;//output compare registers disabled, normal mode
  TCCR3B = 5;//prescaler (0, 1, 8, 64, 256, 1024)
  TCCR3C = 0;//don't for output compare
}

void resetTimer()
{
 // TIFR3 = 0; //reset interupt flag (17th bit)
  TCNT3H=0;
  TCNT3L=0;
}

unsigned long readTimer()
{
  unsigned long high = TCNT3H;
  unsigned long low = TCNT3L;
  //unsigned long hhigh = 0;//(TIFR3 | TOV3);//get interupt flag (seventeenth bit);
  unsigned long result = /*(hhigh << 17) |*/ (high << 8) | low;
  return result;
}
double readTimer_ms()
{
  unsigned long high = TCNT3H;
  unsigned long low = TCNT3L;
  //unsigned long hhigh = 0;//(TIFR3 | TOV3);//get interupt flag (seventeenth bit);
  unsigned long pre_result = /*(hhigh << 17) |*/ (high << 8) | low;
  double result = pre_result/(TICKS_PER_SEC/1000.0);
  return result;
}

double toMS(int count)
{
  return count/(TICKS_PER_SEC/1000.0);
}

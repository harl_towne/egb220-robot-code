#include "Motor.h"

void setupMotors(void) {
  DDRB |= (1 << 5) | (1 << 6);
  //TIMSK1 |= (1 << TOIE1);
  TCCR1A |= (1 << COM1A1) | (1 << COM1B1) | (1 << WGM10) | 1;
  TCCR1B |= (1 << WGM12) | (1 << CS12);
  OCR1A = 0;
  OCR1B = 0;

  TCNT1 = 0;
}

void driveMotor(uint8_t motor, int accel)
{
  if(accel > 255)
  {
    accel = 255;
  }
  if(accel < 0)
  {
    accel = 0;
  }
  
  if (motor == LEFT) {
    OCR1A = accel;
  } else if (motor == RIGHT) {
    OCR1B = accel;
  }
  return;
}

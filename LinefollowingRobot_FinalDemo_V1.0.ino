#include <avr/io.h>
#include <math.h>
#include "Sensors.h"
#include "LCD.h"
#include "Serial.h"
#include "Motor.h"
#define F_CPU 16000000UL
#define N_MOD 100
#define COLOUR 100
#define LBLACK 210
#define RBLACK 210

//state constants
const uint8_t slowSpeed = 80;
const uint8_t straightSpeed = 105;
const uint8_t startSpeed = 130;
const uint8_t slowZoneSpeed = 80;
const float turnThreshold = 0.75;
// PID constants
const float Kp = 330; //propotional
const float Ki = 0;//30;//;20;//1.2*Ku/Tu; //integral
const float Kd = 10;//20;//3*Ku*Tu/40; //derivative
const float sKp = 1; //speed slow down

//PID related constants
int targetSpeed = startSpeed; //average speed
const double sensorWeights[] = {-1, -0.714, -0.429, -0.143, 0.143, 0.429, 0.714, 1}; //weighting of each sensor
const int derivativePeriod = 200; // time to averate deriviative over (ms)
const int historyLength = (int)(derivativePeriod*1.3/4); //number of time steps to record (must be long enough for derivativePeriod)

//PID global variables
uint8_t sensorReadings[10];
float integral = 0;
float lastError = 0;
float history[historyLength];
int deltas[historyLength];

//encoder
uint8_t lEncoderState;
uint8_t rEncoderState;
unsigned int lastEncoderLeft = 0;
unsigned int lastEncoderRight = 0;
unsigned int lastTickThreshold = 15625/2;
unsigned int leftDist = 0;
unsigned int rightDist = 0;

//mapping
unsigned int segmentDistanceDelay = 20;

//0 - no reading, 1 - left reading (segment ticks), 2 - right reading (start/stop tick), 3 - both (intersection, 4 - start slow zone, 5, - stop slow zone
uint8_t currentOutriggerState = 0; 
uint8_t slowZoneState = 0;


int main()
{
  serialStart(74880);
  sensorsInit();
  setupMotors();
  timerInit();
  i2cInit();
  lcdInit();
  lcdClear();
  
  //to prevent NaN errors
  for(int i = 0; i < historyLength; i++)
  {
    history[i] = 0;
    deltas[i] = 0;
  }

  
  //set all these from sensors
  lEncoderState = getLEncoderState();
  rEncoderState = getREncoderState();

  DDRD |= (1<<5);
  DDRC |= (1<<7) | (1<<6);
  
  lcdClear();
  lcdCursor(1, 0);
  lcdPrint("Dist:");
  lcdCursor(0, 0);

  float last = 0;
  int n = 0;
  int delta;
  unsigned int totalDist = 0;
  unsigned int printDist = 0;
  unsigned int lastLeftDist = 0;
  unsigned int lastRightDist = 0;
  float lrCompare = 0;
  while(true)
  {
    //dela
    delta = readTimer();
    resetTimer();

    //read sensors
    fillSensorArray();
    //====================mapping + encoders + outriggers ===========================



    //distance/speed
    lastEncoderLeft += delta;
    lastEncoderRight += delta;
    if(lEncoderState != getLEncoderState())
    {
      
      lEncoderState = getLEncoderState();
      lastEncoderLeft = 0;
      leftDist++;
    }
    if(rEncoderState != getREncoderState())
    {
      rEncoderState = getREncoderState();
      lastEncoderRight = 0;
      rightDist++;
    }

    if(getOutriggerState() == 0 && currentOutriggerState != 0)
    {
      if(currentOutriggerState == 5)
      {
        if(slowZoneState)
        {
          slowZoneState = 1;
          PORTC |= (1<<6);
        }
        else
        {
          slowZoneState = 0;
          PORTC &= ~(1<<6);
        }
      }
      switch(currentOutriggerState)
      {
        case 1: //segment tick
          slowZoneState = 0;
          PORTC &= ~(1<<6);
          PORTD ^= (1<<5);
          PORTC &= ~(1<<7);
          targetSpeed = slowSpeed;
          lastLeftDist = leftDist;
          lastRightDist = rightDist;
          break;
          
        case 2: //start/stop tick
          unsigned int last = readTimer();
          driveMotor(LEFT, 0);
          driveMotor(RIGHT, 0);
          while(readTimer()-last < 15625*2);
          resetTimer();
          while(readTimer() < 15625*2);
          break;

        case 3://intersection
          slowZoneState = 0;
          PORTC &= ~(1<<6);
          break;
      }
      currentOutriggerState = 0;      
    }

    

    float tempL = leftDist - lastLeftDist;
    float tempR = rightDist - lastRightDist;
    if((tempL + tempR)/2 > segmentDistanceDelay)
    {
      lrCompare = tempL/tempR;
      if(tempR/tempL < lrCompare)
      {
        lrCompare = tempR/tempL;
      }
      if(lrCompare > turnThreshold)
      {
        PORTC |= (1<<7);
        targetSpeed = straightSpeed;
      }
      else
      {
        PORTC &= ~(1<<7);
        targetSpeed = slowSpeed;
      }
    }

    if(slowZoneState)
    {
      targetSpeed = slowZoneSpeed;      
    }
    
    if(lastEncoderRight > lastTickThreshold && lastEncoderLeft > lastTickThreshold)
    {
      targetSpeed = startSpeed;
    }

    //outrigger state change 
    if(getOutriggerState() != currentOutriggerState)
    {
      /*
       * check to make sure the new state is not a lesser state
        allowed state changes:
        0 -> any
        any -> 3 (overides all other tick marks)
        5 -> 1 (fixes mixed reading when entering a tics)
        4 -> 1 (fixes mixed reading when entering a tics)
      */
      uint8_t newState = getOutriggerState();
      if(currentOutriggerState == 0 ||    //any -> 0
      newState == 3 ||     //any -> 3
      (currentOutriggerState > 3 && newState == 1))//4/5 -> 1
      {
        currentOutriggerState = getOutriggerState();
      }
    }


    //========================== PID =================================
    
    //error/proportional
    float error = 0;
    float sum = 0;
    for(int i = 0; i < 8; i++)
    {
      error += sensorReadings[i] * sensorWeights[i];
      sum += sensorReadings[i];
    }
    error /= sum;

    //integral
    integral += error * toMS(delta)/1000.0;
//    !!!!!!!!!!!!!!!!!!!!improve this i hate it!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if((integral < 0 && error > 0) || (integral > 0 && error < 0))
    {
      integral = 0;
    }

    //derivative
    for(int i = historyLength-1; i > 0; i--)
    {
      history[i] = history[i-1];
      deltas[i] = deltas[i-1];
    }
    history[0] = last - error;
    last = error;
    
    deltas[0] = delta;
    float rise = 0;
    float runn = 0;
    for(int i = 0; i < historyLength; i++)
    {
      rise += history[i];
      runn += deltas[i];
      if(toMS(runn) > derivativePeriod)
      {
        break;
      }
    }
    float derivative = rise/(toMS(runn)/1000.0);
    
    float turn = Kp * error + Ki * integral + Kd * derivative;

    int leftSpeed = targetSpeed - turn;
    int rightSpeed = targetSpeed + turn;
    
    int newTarget = targetSpeed;
    if(turn > 0)
    {
//     newTarget -= error * sKp;
       leftSpeed -= sKp*turn;
    }
    else
    {
//     newTarget += error * sKp; 
      rightSpeed += sKp*turn;
    }
//    if(rightSpeed > 255)
//    {
//      leftSpeed -= (rightSpeed - 255);
//      rightSpeed =255;
//    }
    
    driveMotor(LEFT, leftSpeed);
    driveMotor(RIGHT, rightSpeed);


   //=================== LCD printing ====================
      
    //all prints for lcd must be done accross multiple cycles using n
    //print sensor readings across first line
    if(n%N_MOD == N_MOD-1)
    {
      lcdCursor(0, 0);
    }
//    if(n%N_MOD == 1)
//    {
//      lcdPrint((String)error);
//    }
    if(n%N_MOD <16)
    {
      if(n%2 == 0)
      {
        uint8_t i = (n%N_MOD)/2;
        lcdPrint((int)(sensorReadings[i]*9/255));
      }
      else
      {
        lcdPrint(" ");
      }
    }
    //clear last dist reading
    if(n%N_MOD == 49)
    {
      lcdCursor(1, 6);
    }
    if(n%N_MOD >= 50 && n%N_MOD < 60)
    {
      lcdPrint(" ");
    }
    //print current dist reading
    if(n%N_MOD == 60)
    {
      lcdCursor(1, 15);
      printDist = (leftDist + rightDist)/2;
    }
    if(n%N_MOD >= 70 && n%N_MOD < 80)
    {
      if(n%2 == 0)
      {
        lcdPrint(printDist % 10);
        printDist /= 10;
      }
      else
      {
        lcdCursor(1, (int)(15 - (n%N_MOD - 69)/2));
      }
    }
    n++;
  } 
}

void fillSensorArray()
{
  for(int i = 0; i < 10; i++)
  {
    if(i == 0)
    {
      sensorReadings[9] = readSensor(i);
    }
    else
    {
      sensorReadings[i-1] = readSensor(i);
    }
  }
}

uint8_t getOutriggerState()
{
  uint8_t left = sensorReadings[8];
  uint8_t right = sensorReadings[9];
  if(right > RBLACK && (left > LBLACK || left > COLOUR))
  {
    return 3;
  }
  if(left > LBLACK)
  {
    return 1;
  }
  if(right > RBLACK)
  {
    return 2;
  }
  if(left > COLOUR)
  {
    return 5;
  }
  return 0;
}

uint8_t getLEncoderState()
{
  return readSensor(10);
}

uint8_t getREncoderState()
{
  return readSensor(11);
}

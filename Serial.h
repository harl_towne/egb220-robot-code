#ifndef SERIAL_H
#define SERIAL_H

void serialStart(unsigned long baud);
void serialSend(String message);
void serialSend(int message);
void i2cInit();

#endif 

#include "Sensors.h"
//MUX codes for each of the sensors - change this to change ADCs or change sensor position
//const uint8_t muxCodes[] = {1, 4, 5, 6, 7, 0b00100000, 0b00100001, 0}; //0, 1, 4, 5, 6, 7, 8, 9
//const uint8_t muxCodes[] = {0b00000000, 0b00000001, 0b00000100, 0b00000101, 0b00000110, 0b00000111, 0b00100000, 0b00100001, 0b00100010, 0b00100011, 0b00100100, 0b00100101}; 
//0, 1, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13
const uint8_t muxCodes[] = {4, 5, 6, 7, 0b00100011, 0b00100010, 0b00100001, 0b00100000, 0, 1};// 4, 5, 6, 7, 11, 10, 9, 8, 0, 1 (last two are outriggers)
//12 - 0b00100100
//13 - 0b00100101
void sensorsInit() {
  // set up ADC
  // enable adc, enable auto-trigger, 128 pre-scalar
  ADCSRA |= (1 << ADEN) | (1 << ADATE) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
  // free-running mode
  ADCSRB = 0;

  //setup digital pins for encoders
  //e6, c7, c6, b7?, b2!, b3!, b1, b0
  DDRB &= ~(1 << 1)  & ~(1 << 2);
  PORTB &= ~(1 << 1)  & ~(1 << 2);
}

void setupADMUX(int sensor_no) {
//  uint8_t muxCode = muxCodes[ADC_channelArray[sensor_no]];
  //set adc
  ADCSRB = 0b00100000 & muxCodes[sensor_no];
  ADMUX = (1 << ADLAR) | (1 << REFS0) | (muxCodes[sensor_no] & 0b00011111); //Select ADC Channel

//  // start conversion
  ADCSRA |= (1 << ADSC);
}


/*
 * sensors 0-7: sensor bar
 * sensors 8, 9: outriggers
 * sensors 10, 11: encoders (digital)
 */
uint8_t readSensor(int sensor_no) {

  //read encoders
  if(sensor_no > 9)
  {
    if(PINB & (1<<(sensor_no-9)))
    {
      return 1;
    }
    return 0;
  }

  //read everything else
  // call setup ADMUX function to vary the sensor being read
  setupADMUX(sensor_no);
  // wait until conversion is finished
  while (~ADCSRA & (1 << ADIF)) {}
  // read value from ADC for each sensor and write into array
  uint8_t sensorOut = ADCH;
  return sensorOut;
}

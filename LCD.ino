#include "LCD.h"

#define RS 0
#define RW 1
#define EN 2
#define PW 3
#define D0 0
#define D1 1
#define D2 2
#define D3 3
#define D4 4
#define D5 5
#define D6 6
#define D7 7
//data byte is: RS, RW, EN, PW, D0/D4, D1/D5, D2/D6, D3/D7

//===============LCD Functions==========================
void lcdInit()
{
  //initialise 4 bit mode
  start();
  toggle((1 << D5));
  stop();

  //fix mode register
  lcdCommand((1 << D5) | (1 << D3));

  //display cursor on
  lcdCommand((1 << D3) | (1 << D2));

  //auto increment on
  lcdCommand((1 << D2) | (1 << D1));

  //clear display
  lcdCommand((1 << D0));

  //go home
  lcdCommand((1 << D7));;
}

void lcdCommand(uint8_t command)
{
  start();
  toggle(command & 0xF0);
  toggle((command << 4));
  stop();
}

void lcdData(uint8_t data)
{

  start();
  toggle((data & 0xF0) | (1 << RS));
  toggle((data << 4) | (1 << RS));
  stop();
}

void lcdPrint(String message)
{
  //convert string to char array
  int len = message.length();
  char m_array[len + 1];
  strcpy(m_array, message.c_str());
  //send each char individualy
  for (int i = 0; i < len; i++)
  {
    lcdData(m_array[i]);
  }
}

void lcdPrint(int message)
{
  lcdPrint((String)message);
}

void lcdCursor(uint8_t row, uint8_t col)
{
  row = row % 2;
  col = col % 16;
  uint8_t ddram_address = row * 0x40 + col;
  lcdCommand(ddram_address | (1 << D7));
}

void lcdClear()
{
  lcdCommand(0x01);
}

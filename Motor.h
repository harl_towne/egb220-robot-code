#ifndef MOTOR_H
#define MOTOR_H
#define LEFT 0
#define RIGHT 1

void setupMotors(void);
void driveMotor(uint8_t motor, uint8_t accel);

#endif 

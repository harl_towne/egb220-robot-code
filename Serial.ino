#include "Serial.h"

#define address 78

//===============UART Functions==========================
//initialse UART
void serialStart(unsigned long baud)
{
  unsigned long UBRR = 16000000 / (16 * baud) - 1;
  /* Set baud rate */
  UBRR1H = (unsigned char)(UBRR >> 8);
  UBRR1L = (unsigned char)UBRR;
  /* Enable receiver and transmitter */
  UCSR1B = (1 << RXEN1) | (1 << TXEN1);
  /* Set frame format: 8data, 2stop bit */
  UCSR1C = (1 << USBS1) | (3 << UCSZ10);
}

//send string. function doesn't return untill sending finishes
void serialSend(String message)
{
  //convert string to char array
  int len = message.length();
  char m_array[len + 1];
  strcpy(m_array, message.c_str());
  //send each char individualy
  for (int i = 0; i < len; i++)
  {
    while ( !(UCSR1A & (1 << UDRE1)));
    UDR1 = m_array[i];
  }
}

//send int. function doesn't return untill sending finishes
void serialSend(int message)
{
  //convert int to string and use previous function
  serialSend((String)message);
}

//===============I2C Functions==========================
void i2cInit()
{
  //set SCL to 100kHz
  TWSR = 0x00; //status register
  TWBR = 18; //bit rate register

  //enable TWI
  TWCR = (1 << TWEN);
}

void start()
{
  //  PORTD |= (1<<5);
  uint8_t curr_status = 0;
  TWCR = (1 << TWSTA) | (1 << TWEN) | (1 << TWINT); /* Enable TWI, generate START */
  while (!(TWCR & (1 << TWINT))); /* Wait until TWI finish its current job */
  curr_status = TWSR & 0xF8; /* Read TWI status register */
  if (curr_status != 0x08)  /* Check weather START transmitted or not? */
  {
    serialSend("start ERROR\n");
  }
  TWDR = address;  /* Write SLA+W in TWI data register */
  TWCR = (1 << TWEN) | (1 << TWINT); /* Enable TWI & clear interrupt flag */
  while (!(TWCR & (1 << TWINT))); /* Wait until TWI finish its current job */
  curr_status = TWSR & 0xF8; /* Read TWI status register */
  if (curr_status != 0x18) /* Check for SLA+W transmitted &ack received */
  {
    serialSend("SLA+W ERROR\n");
  }
}

void data(uint8_t data)
{
  uint8_t curr_status = 0;
  TWDR = data;      /* Copy data in TWI data register */
  TWCR = (1 << TWEN) | (1 << TWINT); /* Enable TWI and clear interrupt flag */
  while (!(TWCR & (1 << TWINT))); /* Wait until TWI finish its current job */
  curr_status = TWSR & 0xF8; /* Read TWI status register */
  if (curr_status != 0x28) /* Check for data transmitted &ack received */
  {
    serialSend("data ERROR\n");
  }
}

void toggle(uint8_t data)
{
  uint8_t curr_status = 0;
  TWDR = data | (1 << EN) | (1 << PW); /* Copy data in TWI data register */
  TWCR = (1 << TWEN) | (1 << TWINT); /* Enable TWI and clear interrupt flag */
  while (!(TWCR & (1 << TWINT))); /* Wait until TWI finish its current job */
  curr_status = TWSR & 0xF8; /* Read TWI status register */
  if (curr_status != 0x28) /* Check for data transmitted &ack received */
  {
    serialSend("data ERROR\n");
  }
  _delay_ms(1);
  TWDR = data | (1 << PW);  /* Copy data in TWI data register */
  TWCR = (1 << TWEN) | (1 << TWINT); /* Enable TWI and clear interrupt flag */
  while (!(TWCR & (1 << TWINT))); /* Wait until TWI finish its current job */
  curr_status = TWSR & 0xF8; /* Read TWI status register */
  if (curr_status != 0x28) /* Check for data transmitted &ack received */
  {
    serialSend("data ERROR\n");
  }
  _delay_ms(5);
}

void stop()
{
  TWCR = (1 << TWSTO) | (1 << TWINT) | (1 << TWEN); /* Enable TWI, generate stop */
  while (TWCR & (1 << TWSTO)); /* Wait until stop condition execution */
  //  PORTD &= ~(1<<5);
}

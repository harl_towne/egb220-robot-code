#ifndef LCD_H
#define LCD_H

void lcdInit();
void lcdCommand(uint8_t command);
void lcdData(uint8_t data);
void lcdPrint(String message);
void lcdPrint(int message);
void lcdCursor(uint8_t row, uint8_t col);
void lcdClear();

#endif 
